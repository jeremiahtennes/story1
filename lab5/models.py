from django.db import models
import datetime
from django.utils import timezone

# Create your models here.
class Jadwal(models.Model):
    nama_kegiatan = models.CharField(max_length=50)
    tempat = models.CharField(max_length=25)
    kategori = models.CharField(null=True, max_length=25)
    thnA = models.CharField(null=True, max_length=25)
    sks = models.CharField(null=True, max_length=25)
    descrip = models.CharField(null=True, max_length=100)
    tanggal = models.DateField(default=timezone.now)
    waktu = models.TimeField(default=timezone.now)

    def __str__(self):
        return self.nama_kegiatan
