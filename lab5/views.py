from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import JadwalForm
from .models import Jadwal
from django.utils import timezone
import datetime

# Create your views here.
def create_jadwal(request):
    form = JadwalForm()
    if request.method =="POST":
        form=JadwalForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('lab5:jadwal')
    return render(request, "lab5/form.html", {'form':form})

def details(request, id):
    jadwal = Jadwal.objects.get(id=id)
    response = {
        'jadwal' : jadwal,
        'server_time' : datetime.datetime.now(),
    }
    return render(request, "lab5/details.html", response)

def jadwal(request):
    jadwals = Jadwal.objects.order_by("tanggal", "waktu")
    response = {
        'jadwal' : jadwals,
        'server_time' : datetime.datetime.now(),
    }
    return render(request, "lab5/jadwal.html", response)

def delete(request):
    if request.method == "POST":
        id = request.POST['id']
        Jadwal.objects.get(id=id).delete()
    return redirect('lab5:jadwal')