# Generated by Django 2.2 on 2019-10-01 18:28

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab5', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='jadwal',
            name='tanggal',
            field=models.DateField(default=datetime.datetime(2019, 10, 1, 18, 28, 4, 131378)),
        ),
        migrations.AlterField(
            model_name='jadwal',
            name='waktu',
            field=models.TimeField(default=datetime.datetime(2019, 10, 1, 18, 28, 4, 131426)),
        ),
    ]
