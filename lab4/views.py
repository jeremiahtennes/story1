from django.shortcuts import render
import datetime

# Create your views here.
def lab1(request):
    return render(request, "lab1.html")

def aboutpage(request):
    return render(request, "aboutpage.html")

def contact(request):
    return render(request, "contact.html")  

def extra(request):
    return render(request, "extra.html")  
    
def experience(request):
    return render(request, "experience.html")  

def skill(request):
    return render(request, "skill.html")  

