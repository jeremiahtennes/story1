from django.contrib import admin
from django.urls import path, register_converter
from . import views, converters
from django.http import HttpResponse
import datetime


app_name = 'lab4'

register_converter(converters.NegativeIntConverter, 'negint')

urlpatterns = [
    path('', views.aboutpage),
    path('aboutpage', views.aboutpage),
    path('contact', views.contact),
    path('lab1', views.lab1),
    path('extra', views.extra),
    path('experience', views.experience),
    path('skill', views.skill),
]